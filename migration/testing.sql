USE
	[Plasma_Dev]

/* model validation */

DELETE
	dbo.Plasma_Migration_Models
INSERT
	dbo.Plasma_Migration_Models
SELECT DISTINCT
	COUNT(DISTINCT(a.PRDHA)) AS test,
	b.ZZ_PLANMOD
FROM
	(SELECT * FROM SAP_Master.dbo.MARA a LEFT JOIN SAP_Master.dbo.MARA_ADDITIONAL b ON a.MATNR = b.Material WHERE LEFT (b.[Current Season], 4) IN ('2017', '2016')) a
LEFT JOIN
	SAP_Master.dbo.MARA b
ON
	a.ZZ_ARTICLE = b.MATNR
LEFT JOIN
	SAP_Master.dbo.MARA c
ON
	b.ZZ_PLANMOD = c.MATNR
WHERE
	a.PRDHA = b.PRDHA OR b.PRDHA IS NULL
	AND a.PRDHA = c.PRDHA OR c.PRDHA IS NULL
	AND a.MTART = 'FERT'
	AND b.ZZ_PLANMOD IS NOT NULL
GROUP BY
	b.ZZ_PLANMOD

DELETE
	dbo.Plasma_Migration_Models
WHERE
	ZZ_PLANMOD IS NULL
	OR test NOT IN ('1')

/* end model validation */

/* prhda validation */

DELETE
	[dbo].[Plasma_Migration_Prdha]
INSERT
	[dbo].[Plasma_Migration_Prdha]
SELECT DISTINCT
	a.PRDHA,
	COUNT(DISTINCT(b.ZZ_PLANMOD)) AS test
FROM
	(SELECT * FROM SAP_Master.dbo.MARA a LEFT JOIN SAP_Master.dbo.MARA_ADDITIONAL b ON a.MATNR = b.Material WHERE LEFT (b.[Current Season], 4) IN ('2017', '2016')) a
LEFT JOIN
	SAP_Master.dbo.MARA b
ON
	a.ZZ_ARTICLE = b.MATNR
LEFT JOIN
	SAP_Master.dbo.MARA c
ON
	b.ZZ_PLANMOD = c.MATNR
WHERE
	a.PRDHA = b.PRDHA OR b.PRDHA IS NULL
	AND a.PRDHA = c.PRDHA OR c.PRDHA IS NULL
	AND a.MTART = 'FERT'
	AND b.ZZ_PLANMOD IS NOT NULL
GROUP BY
	a.PRDHA

DELETE
	[dbo].[Plasma_Migration_Prdha]
WHERE
	PRDHA IS NULL
	OR test NOT IN ('1')

/* end prhda validation */

/* article validation */
DELETE
	[dbo].[Plasma_Migration_Article]
INSERT
	[dbo].[Plasma_Migration_Article]
SELECT DISTINCT
	COUNT(DISTINCT(MATNR)) AS test,
	ZZ_ARTICLE
FROM
	SAP_Master.dbo.MARA
GROUP BY
	ZZ_ARTICLE

DELETE
	[dbo].[Plasma_Migration_Article]
WHERE
	ZZ_ARTICLE IS NULL
	OR test NOT IN ('1')

/* main query */

/* migration fields */

SELECT
	'SU' AS PRODUCT_LINE,
	'AL17' AS CURRENT_SEASON,
	b.ZZ_PLANMOD AS PLANNING_MODEL,
	CAST('00' AS VARCHAR) AS SPORT_CATEGORY,
	PROJECT = CASE
		WHEN e.MAKTX LIKE '%AMBIT%' THEN 'LAITURI'
		WHEN e.MAKTX LIKE '%SPARTAN%' THEN 'NG'
		WHEN e.MAKTX LIKE '%CORE%' THEN 'CORE'
		WHEN e.MAKTX LIKE '%EON%' THEN 'GURU'
		WHEN e.MAKTX LIKE '%ZOOP%' THEN 'BIG DISPLAY'
		WHEN e.MAKTX LIKE '%TRAVERSE%' THEN 'KEIJU'
		WHEN e.MAKTX LIKE '%BELT%' THEN 'SENSOR'
	END,
	FAMILY = CASE
		WHEN a.PRDHA LIKE '%T%' THEN 'PERFORMANCE'
		WHEN a.PRDHA LIKE '%D%' THEN 'DIVE'
		WHEN RIGHT(LEFT(a.PRDHA, 8), 2) IN ('0W', '20', '10') THEN 'OUTDOOR_LIFESTYLE'
	END,
	e.MAKTX AS PRODUCT_NAME,
	'U' AS GENDER,
	a.PRDHA AS PROD_HIERARCHY,
	SIZE_TABLE = CASE
		WHEN d.MAKTX LIKE '%BELT M%' THEN 'S-M-L'
		WHEN d.MAKTX LIKE '%BELT L%' THEN 'S-M-L'
		WHEN d.MAKTX LIKE '%BELT S%' THEN 'S-M-L'
		ELSE ''
	END,
	'' AS LABEL_TABLE,
	a.[Product type] AS PRODUCT_TYPE,
	CC_EU = CASE
		WHEN j.CC_EU IS NULL THEN ''
		ELSE j.CC_EU
	END,
	CC_JP = CASE
		WHEN m.CC_JP IS NULL THEN ''
		ELSE m.CC_JP
	END,
	CC_KO = CASE
		WHEN n.CC_KO IS NULL THEN ''
		ELSE n.CC_KO
	END,
	CC_CA = CASE
		WHEN p.CC_CA IS NULL THEN ''
		ELSE p.CC_CA
	END,
	a.ZZ_ARTICLE AS PLANNING_ARTICLE,
	f.MAKTX AS ARTICLE_DESCRIPTION,
	a.[Usage] AS ARTICLE_TYPE,
	COLOR_CODE = CASE
		WHEN a.[Color description] IN ('N/A', NULL) THEN CASE
			WHEN d.MAKTX LIKE '%AMBER%' THEN 'AMBER'
			WHEN d.MAKTX LIKE '%AQUA%' THEN 'AQUA'
			WHEN d.MAKTX LIKE '%BLACK%' THEN 'BLACK'
			WHEN d.MAKTX LIKE '%BLUE%' THEN 'BLUE'
			WHEN d.MAKTX LIKE '%COPPER%' THEN 'COPPER'
			WHEN d.MAKTX LIKE '%CORAL%' THEN 'CORAL'
			WHEN d.MAKTX LIKE '%DIAMOND%' THEN 'DIAMOND'
			WHEN d.MAKTX LIKE '%FUCHSIA%' THEN 'FUCHSIA'
			ELSE ''
			END
		ELSE a.[Color description]
	END,
	'' AS COLOR_CODE2,
	'' AS COLOR_CODE3,
	q.ZZ_RETAILDATE AS RID_EMEA,
	r.HERKL AS ORIGIN,
	'' AS FACTORY,
	'' AS LEAD_TIME,
	'UN' AS UOM,
	SIZE = CASE
		WHEN d.MAKTX LIKE '%BELT M%' THEN 'M'
		WHEN d.MAKTX LIKE '%BELT L%' THEN 'L'
		WHEN d.MAKTX LIKE '%BELT S%' THEN 'S'
		ELSE ''
	END,
	a.MATNR AS SKU_CODE,
	d.MAKTX AS SKU_DESCRIPTION,
	s.UPC,
	s.EAN,
	a.GROES AS GROSS_WEIGHT,
	'' AS MASTER_CARTON,
	'1' AS QTY_PER_CARTON,
	'Regular 63' AS MANAGEMENT_CODE	

/* migration fields */

FROM
	(SELECT * FROM SAP_Master.dbo.MARA a LEFT JOIN SAP_Master.dbo.MARA_ADDITIONAL b ON a.MATNR = b.Material WHERE LEFT (b.[Current Season], 4) IN ('2017', '2016')) a
LEFT JOIN
	SAP_Master.dbo.MARA b
ON
	a.ZZ_ARTICLE = b.MATNR
LEFT JOIN
	SAP_Master.dbo.MARA c
ON
	b.ZZ_PLANMOD = c.MATNR
/* sku description */
LEFT JOIN
	(SELECT * FROM SAP_Master.dbo.MAKT WHERE SPRAS_ISO = 'EN') d
ON
	a.MATNR = d.MATNR
/* model description */
LEFT JOIN
	(SELECT * FROM SAP_Master.dbo.MAKT WHERE SPRAS_ISO = 'EN') e
ON
	b.ZZ_PLANMOD = e.MATNR
/* article description */
LEFT JOIN
	(SELECT * FROM SAP_Master.dbo.MAKT WHERE SPRAS_ISO = 'EN') f
ON
	a.ZZ_ARTICLE = f.MATNR
/* article, prdha and model validation */
INNER JOIN
	dbo.Plasma_Migration_Models g
ON
	b.ZZ_PLANMOD COLLATE DATABASE_DEFAULT = g.ZZ_PLANMOD COLLATE DATABASE_DEFAULT

INNER JOIN
	[dbo].[Plasma_Migration_Prdha] h
ON
	a.PRDHA COLLATE DATABASE_DEFAULT = h.PRDHA COLLATE DATABASE_DEFAULT

INNER JOIN
	[dbo].[Plasma_Migration_Article] k
ON
	a.ZZ_ARTICLE COLLATE DATABASE_DEFAULT = k.ZZ_ARTICLE COLLATE DATABASE_DEFAULT

/* end article, prhda and model validation */

/* commodity codes */

LEFT JOIN
	(SELECT
		MATNR,
		STAWN AS CC_EU
	FROM
		SAP_Master.dbo.MARC
	WHERE
		WERKS = '5050'
	) j

ON
	a.MATNR = j.MATNR

LEFT JOIN
	(SELECT
		MATNR,
		STAWN AS CC_US
	FROM
		SAP_Master.dbo.MARC
	WHERE
		WERKS = '8800'
	) t

ON
	a.MATNR = t.MATNR

LEFT JOIN
	(SELECT
		MATNR,
		STAWN AS CC_JP
	FROM
		SAP_Master.dbo.MARC
	WHERE
		WERKS = '6100'
	) m

ON
	a.MATNR = m.MATNR

LEFT JOIN
	(SELECT
		MATNR,
		STAWN AS CC_KO
	FROM
		SAP_Master.dbo.MARC
	WHERE
		WERKS = '6500'
	) n

ON
	a.MATNR = n.MATNR

LEFT JOIN
	(SELECT
		MATNR,
		STAWN AS CC_CA
	FROM
		SAP_Master.dbo.MARC
	WHERE
		WERKS = '9100'
	) p
ON
	a.MATNR = p.MATNR

/* end commodity codes */

/* RID emea */

LEFT JOIN

	(SELECT
		*
	FROM
		(SELECT
			*,
			ROW_NUMBER() OVER (PARTITION BY MATNR ORDER BY ZZ_RETAILDATE DESC) AS rn

		FROM
			SAP_Master.dbo.MVKE
		WHERE
			VKORG IN
			('1100',
			'1300',
			'1400',
			'1600',
			'1700',
			'1800',
			'2100',
			'2300',
			'2400',
			'2600',
			'2640',
			'3000',
			'3300',
			'3400',
			'3500',
			'3600',
			'3700',
			'4800',
			'4900',
			'5310',
			'5000',
			'3410',
			'4110')

		) a
	WHERE
		a.rn = 1
		AND ZZ_RETAILDATE IS NOT NULL
	) q

ON
	a.MATNR = q.MATNR

/* end RID emea */

/* origin */

LEFT JOIN

	(SELECT
		MATNR,
		HERKL
	FROM
		SAP_Master.dbo.MARC
	WHERE
		WERKS = 'SUF1'
	) r

ON
	a.MATNR = r.MATNR

/* end origin */

/* UPC, EAN */

LEFT JOIN

	(SELECT
		a.MATNR,
		a.EAN,
		b.UPC
	FROM
		(SELECT
			MATNR,
			EAN11 AS EAN
		FROM
			SAP_Master.dbo.MEAN
		WHERE
			EANTP = 'SI'
		) a
	LEFT JOIN
		(SELECT
			MATNR,
			EAN11 AS UPC
		FROM
			SAP_Master.dbo.MEAN
		WHERE
			EANTP = 'SZ'
		) b
	ON
		a.MATNR = b.MATNR
	) s

ON
	a.MATNR = s.MATNR

/* end UPC, EAN */

WHERE
	a.PRDHA = b.PRDHA OR b.PRDHA IS NULL
	AND a.PRDHA = c.PRDHA OR c.PRDHA IS NULL
	AND a.MTART = 'FERT'
	AND g.ZZ_PLANMOD IS NOT NULL